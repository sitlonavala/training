public class CompanyInheritance {

	public static void main(String[] args) {
		EmployeeInheritance employee1 = new EmployeeInheritance(1111, "Sunil", 15000.0, 101,"Sales", "Noida");
		EmployeeInheritance employee2 = new EmployeeInheritance(1112, "Tejas", 35000.0, 101, "Sales", "Mumbai");
		EmployeeInheritance employee3 = new EmployeeInheritance(1113, "Sanket", 80000, 201, "Production", "Pune");
		EmployeeInheritance employee4 = new EmployeeInheritance(1114,  "Kritika", 20000, 301, "Admin", "Chennai");
		
		System.out.println("Employee 1 : ");
		System.out.println(employee1);
		System.out.println();
		
		System.out.println("Employee 2 : ");
		System.out.println(employee2);
		System.out.println();
		
		System.out.println("Employee 3 : ");
		System.out.println(employee3);
		System.out.println();
		
		System.out.println("Employee 4 : ");
		System.out.println(employee4);
		System.out.println();
	}

}
