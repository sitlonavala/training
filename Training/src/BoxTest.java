public class BoxTest {

	public static void main(String[] args) {
		Box box1    = new Box();
		box1.width  = 23;
		box1.height = 19;
		box1.depth  = 13;
		
		System.out.println("Box 1 : ");
		System.out.println(box1);
		System.out.println("Volume  : "  + box1.volume());
		System.out.println();
		
		Box box2 = new Box(12,8,46);
		System.out.println("Box 2 : ");
		System.out.println(box2);
		System.out.println("Volume  : "  + box2.volume());
		System.out.println();
		
		Box box3 = new Box();
		System.out.println("Box 3 : ");
		System.out.println(box3);
		System.out.println("Volume  : "  + box3.volume());
		System.out.println();
		
		Box box4 = new Box(23);
		System.out.println("Box 4 : ");
		System.out.println(box4);
		System.out.println("Volume  : "  + box4.volume());
		System.out.println();
		
		Box box5 = new Box(box2);
		box5.width = 10;
		System.out.println("Box 5 : ");
		System.out.println(box5);
		System.out.println("Volume  : "  + box5.volume());
		System.out.println();
		
		Box box6 = box5.doubleBox();
		System.out.println("Box 6 : ");
		System.out.println(box6);
		System.out.println("Volume  : "  + box6.volume());
		System.out.println();
	}

}
