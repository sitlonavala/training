public class OverloadingTest {

	public static void main(String[] args) {
		MethodOverloading object = new MethodOverloading();
		int firstInteger = 78;
		int secondInteger = 56;
		
		double firstDouble = 34.8;
		double secondDouble = 23.87;
		
		System.out.println("Addition of Integer = " + object.sum(firstInteger, secondInteger));
		System.out.println("Addition of Double = " + object.sum(firstDouble, secondDouble));
	}

}
