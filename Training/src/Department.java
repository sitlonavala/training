public class Department {
	int    deptID;
	String deptName;
	String location;
	
	public Department() {}
	
	public Department(int deptID, String deptName, String location) {
		this.deptID   = deptID;
		this.deptName = deptName;
		this.location = location;
	}
}
