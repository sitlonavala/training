public class Box {
	int width;
	int height;
	int depth;
	
	Box(){}
	
	Box(int width, int height, int depth) {
		this.width  = width;
		this.height = height;
		this.depth  = depth;
	}
	
	Box(int value) {
		width = height = depth = value;
	}
	
	Box(Box box) {
		this.width  = box.width;
		this.depth  = box.depth;
		this.height = box.height;
	}
	
	public int volume() {
		return(width * height * depth);
	}
	
	public Box doubleBox() {
		return(new Box(2 * width, 2 * height, 2 * depth));
	}
	
	@Override
	public String toString() {
		return("[" + "Height = " + height + ", Width = " + width + ", Depth = " + depth + "]");
	}
}
