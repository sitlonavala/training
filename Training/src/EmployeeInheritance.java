public class EmployeeInheritance extends Department {
	int id;
	String name;
	double salary;
	
	public EmployeeInheritance(int id, String name, double salary, int deptID, String deptName, String location) {
		super(deptID,deptName,location);
		this.id 	    = id;
		this.name 	    = name;
		this.salary     = salary;
	}
	
	@Override
	public String toString() {
		return("[" + "id: " + id + ", name: " + name + ", salary: " + salary + ", department: [" + "id: " + deptID + ", name: " + deptName + ", location: " + location + "]" + " ]");
	}
}
