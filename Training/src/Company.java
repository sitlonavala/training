public class Company {

	public static void main(String[] args) {
		Department sales 	  = new Department(301,"Sales","Delhi");
		Department admin 	  = new Department(101, "Admin", "Mumbai");
		Department production = new Department(401, "Production", "Chennai");
		
		Employee employee1 = new Employee(1111, "Sunil", 15000.0, sales);
		Employee employee2 = new Employee(1112, "Tejas", 35000.0, sales);
		Employee employee3 = new Employee(1113, "Sanket", 80000, production);
		Employee employee4 = new Employee(1114,  "Kritika", 20000, admin);
		
		System.out.println("Employee 1 : ");
		System.out.println(employee1);
		System.out.println();
		
		System.out.println("Employee 2 : ");
		System.out.println(employee2);
		System.out.println();
		
		System.out.println("Employee 3 : ");
		System.out.println(employee3);
		System.out.println();
		
		System.out.println("Employee 4 : ");
		System.out.println(employee4);
		System.out.println();

	}

}
