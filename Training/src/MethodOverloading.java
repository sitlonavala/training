public class MethodOverloading {
	public int sum(int firstNumber, int secondNumber) {
		return(firstNumber + secondNumber);
	}
	public double sum(double firstNumber, double secondNumber) {
		return(firstNumber + secondNumber);
	}

}
