public class Employee {
	int id;
	String name;
	double salary;
	Department department;
	
	Employee(int id, String name, double salary, Department department) {
		this.id 	    = id;
		this.name 	    = name;
		this.salary     = salary;
		this.department = department;
	}
	
	@Override
	public String toString() {
		return("[" + "id: " + id + ", name: " + name + ", salary: " + salary + ", department: [" + "id: " + department.deptID + ", name: " + department.deptName + ", location: " + department.location + "]" + " ]");
	}
}
